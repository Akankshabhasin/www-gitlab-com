# frozen_string_literal: true

require 'open-uri'
require 'gitlab'
require 'cgi'

require_relative '../gitlab_api_helper'
require_relative '../stage'
require_relative './styled_yaml'
require_relative '../team'

WWW_GITLAB_COM_PROJECT_ID = 7764

module ReleasePosts
  class PostEntryCollection
    include Helpers

    attr_reader :options

    def initialize(options)
      @options = options
    end

    def execute
      issueable_urls = [options.issue_url].reject(&:empty?)
      issueables = []

      if issueable_urls.empty?
        TYPES.each do |type|
          # TODO: search for other issueables like epics or merge requests
          issueable_urls.push("https://gitlab.com/api/v4/groups/9970/issues?labels=#{type.label}")
        end
      end

      issueable_urls.each do |u|
        puts "Fetching issuables: #{u}"
        body = URI.open(u, 'Accept' => 'application/json').read
        issues = [JSON.parse(body)].flatten

        puts "Found #{issues.length} issuables"
        issueables = [issueables, issues].flatten
      end

      issueables.each do |i|
        i = api_issueable(i['web_url']) unless i['_links']

        next unless i

        issue = Issue.new(i, options.type)
        ReleasePosts::PostEntry.new(issue, @options).execute
      end
    end

    private

    def api_issueable(web_url)
      project_id = CGI.escape(web_url.split('/-/').first.delete_prefix('/'))
      issue_iid = web_url.split('/').last

      u = "https://gitlab.com/api/v4/projects/#{project_id}/issues/#{issue_iid}"
      puts "Upgrading web to api response: #{web_url} -> #{u}"
      body = URI.open(u, 'Accept' => 'application/json').read
      res = [JSON.parse(body)].flatten

      return res.first unless res.length != 1

      fail_with "Unexpected response length. Too many items"
    end
  end

  class PostEntry
    include Helpers

    attr_reader :options

    def initialize(issue, options)
      @options = options
      @issue = issue
    end

    def execute
      unless options.local == false
        assert_feature_branch!
        assert_new_file!
      end

      # Read type from $stdin unless it is already set
      @block_type = @issue.type_name || InputParser.read_type
      assert_valid_type!

      return write_api if options.local == false

      write_local
    end

    private

    def write_api
      $stdout.puts "\n\e[32mcreate\e[0m #{file_path} on branch #{branch}"
      $stdout.puts contents
      commit = [{ action: 'create', file_path: file_path, content: contents }]

      if @issue.image_url
        $stdout.puts "\n\e[32mcreate\e[0m #{image_path}"
        commit.push({ action: 'create', file_path: image_path, content: @issue.image_base64, encoding: 'base64' })
      end

      merge_request = {
        source_branch: branch,
        target_branch: DEFAULT_BRANCH,
        milestone_id: @issue.milestone_id,
        labels: [@issue.stage_label, @issue.group_label, 'release post', 'release post item', 'Technical Writing'],
        remove_source_branch: true
      }

      $stdout.puts "\n\e[32mcreate\e[0m merge request #{merge_request}"
      merge_request['description'] = mr_description

      return if options.dry_run

      begin
        c = Gitlab.create_commit(WWW_GITLAB_COM_PROJECT_ID, branch, @issue.title, commit, start_branch: DEFAULT_BRANCH)
        $stdout.puts "\n\e[32mcommit\e[0m #{c.id} (#{branch})"
      rescue Gitlab::Error::BadRequest => e
        return unless e.message.include?("A branch called '#{branch}' already exists.")

        $stdout.puts "\n\e[33mcommit\e[0m #{branch} already exists. Attempting to continue..."
      end

      begin
        mr = Gitlab.create_merge_request(WWW_GITLAB_COM_PROJECT_ID, @issue.title, merge_request)
        $stdout.puts "\n\e[32mmerge request\e[0m #{mr.web_url}"

        Gitlab.create_merge_request_note(WWW_GITLAB_COM_PROJECT_ID, mr.iid,
                                               "/assign @#{pm_username}\n/milestone %\"#{@issue.milestone_title}\"\n")
      rescue Gitlab::Error::Conflict => e
        return unless e.message.include?("Another open merge request already exists for this source branch")

        $stdout.puts "\n\e[33mmerge request\e[0m already exists. Attempting to continue..."
      end

      return unless @issue.release_post_type_label

      Gitlab.edit_issue(@issue.project_id, @issue.iid, { 'add_labels' => RP_DRAFTED_LABEL })
      $stdout.puts "\n\e[32missue\e[0m set label #{RP_DRAFTED_LABEL}"
    end

    def mr_description
      template = File.open('.gitlab/merge_request_templates/Release-Post-Item.md').read

      # Remove slash commands because they aren't evaluated when set via the merge request API
      template = template.lines.reject { |line| line.start_with?('/label ', '/milestone ', '/assign ') }.join

      template = template.sub('`@PM`', "@#{pm_username}") if pm_username
      template = template.sub('`@PMM`', "@#{pmm_username}") if pmm_username
      template = template.sub('`@TW`', "@#{tw_username}") if tw_username
      template = template.sub('`@EM`', "@#{em_username}") if em_username

      template = template.sub('- **Feature Issue:**', "- **Feature Issue:** #{@issue.web_url}")

      puts template if options.verbose

      template
    end

    def contents
      block_type = @block_type
      yml = case block_type
            when 'deprecation'
              deprecation_content
            when 'removal'
              removal_content
            when 'top', 'primary', 'secondary'
              feature_content(block_type)
            else
              fail_with "Unknown content block type '#{block_type}'"
            end

      remove_trailing_whitespace(yml)
    end

    def feature_content(type_name)
      StyledYAML.dump(
        'features' => {
          type_name => [
            {
              'name' => StyledYAML.double_quoted(@issue.title),
              'available_in' => StyledYAML.inline(@issue.available_in),
              'gitlab_com' => true,
              'documentation_link' => StyledYAML.single_quoted(+'https://docs.gitlab.com/ee/#amazing'),
              'image_url' => StyledYAML.single_quoted(image_path.delete_prefix('source/')),
              'reporter' => reporter,
              'stage' => @issue.stage,
              'categories' => @issue.categories,
              'issue_url' => StyledYAML.single_quoted(@issue.web_url),
              'description' => StyledYAML.literal(@issue.problem_to_solve)
            }
          ]
        }
      )
    end

    def deprecation_content
      StyledYAML.dump(
        'deprecations' => {
          'feature_name' => StyledYAML.double_quoted(@issue.title),
          'reporter' => reporter,
          'due' => 'April 22, 2019',
          'issue_url' => StyledYAML.single_quoted(@issue.web_url),
          'description' => StyledYAML.literal(@issue.problem_to_solve)
        }
      )
    end

    def removal_content
      StyledYAML.dump(
        'removals' => {
          'feature_name' => StyledYAML.double_quoted(@issue.title),
          'reporter' => reporter,
          'date_of_removal' => 'April 22, 2019',
          'issue_url' => StyledYAML.single_quoted(@issue.web_url),
          'description' => StyledYAML.literal(@issue.problem_to_solve)
        }
      )
    end

    def write_local
      abs_image_path = File.join(git_repo_path, image_path)
      abs_content_path = File.join(git_repo_path, file_path)

      $stdout.puts "\n\e[32mcreate\e[0m #{abs_image_path}"
      $stdout.puts "\n\e[32mcreate\e[0m #{abs_content_path}"
      $stdout.puts contents
      $stdout.puts "\n\e[34mhint\e[0m remember to stage and commit, before you push!"

      return if options.dry_run

      File.write(abs_file_path, contents)
      File.write(abs_image_path, @issue.image_binary) if @issue.image_url

      system("#{editor} '#{file_path}'") if editor
    end

    def editor
      ENV['EDITOR']
    end

    def assert_feature_branch!
      return unless git_current_branch == DEFAULT_BRANCH

      fail_with "Create a branch first!"
    end

    def assert_new_file!
      return unless File.exist?(file_path)
      return if options.force

      fail_with "#{file_path} already exists! Use `--force` to overwrite."
    end

    def assert_valid_type!
      return unless @block_type && @block_type == InputParser::INVALID_TYPE

      fail_with 'Invalid category given!'
    end

    def file_path
      ext = '.yml'
      base_path = File.join(unreleased_path, @issue.slug)
      +base_path[0..MAX_FILENAME_LENGTH - ext.length] + ext
    end

    def image_path
      return +'/images/unreleased/feature-a.png' unless @issue.image_url

      ext = File.extname(@issue.image_url)
      base_path = File.join(unreleased_images_path, @issue.slug)
      +base_path[0..MAX_FILENAME_LENGTH - ext.length] + ext
    end

    def unreleased_path
      File.join('data', 'release_posts', 'unreleased')
    end

    def unreleased_images_path
      File.join('source', 'images', 'unreleased')
    end

    def branch
      options.branch || "#{reporter}/#{@issue.group}_#{@issue.iid}"
    end

    def username(name)
      Gitlab::Homepage::Team::Member.all!
        .find { |person| person.name == name }&.gitlab
    end

    def username_for_stage_group_role(stage_key, group_key, role_key)
      name = Gitlab::Homepage::Stage.all!
        .find { |s| s.key == stage_key }
        .groups[group_key][role_key]

      username(name)
    end

    def pm_username
      username_for_stage_group_role(@issue.stage, @issue.group, 'pm')
    end

    def pmm_username
      username_for_stage_group_role(@issue.stage, @issue.group, 'pmm')
    end

    def tw_username
      username_for_stage_group_role(@issue.stage, @issue.group, 'tech_writer')
    end

    def em_username
      bem =  username_for_stage_group_role(@issue.stage, @issue.group, 'backend_engineering_manager')
      fem =  username_for_stage_group_role(@issue.stage, @issue.group, 'frontend_engineering_manager')

      return bem if bem && !fem
      return fem if fem && !bem
      return bem if bem && fem
    end

    def reporter
      ENV['GITLAB_USERNAME'] || pm_username
    end

    def remove_trailing_whitespace(yaml_content)
      yaml_content.gsub(/ +$/, '')
    end
  end
end
